# Cloudbeds Frontend 90 minute Live Challenge.

## Overview

#### The 2 main goals of this challenge:
* Assess your level of expertise with React, typescript, and other libraries we used in our shop.
* How you structure and compartmentalize code  associated with its purpose
    *  ### visual-layer :
        * Strictly UI presentational concerns
        * ( can only access the  🔴 **business-layer**)
    *  ### business-layer :
        * business domain logic concerns associated with communicating  Global/Server state to the
          **business-layer**,
        * instances of cross-cutting concerns such as validators, Domain Models, etc.
        * (has access to 🔵 **data-layer**)
    *  ### data-layer :
        * Http Client API Implementation,
        * Domain Services,
        * scaffolding for Data stores, etc.