export {
    propertyDetailResponse
} from "./propertyDetailResponse";

export {
    propertyListResponse
} from "./propertyListResponse";
