export const propertyDetailResponse = {
    "property": {
        "id": 1,
        "hotelName": "Devil's Crossroad Hotel & Spa",
        "location": "789 Ponce De Leon Ave NE Jackson, MS 30306",
        "country": "US",
        "state": "MS",
        "price": "$1250 to $2250",
        "description": "Jackson, newest Art Deco darling, and can be found on the historic Printers Alley in a building that was once home to Noel Place, one of Music City’s premier luxury hotels that opened in 1930",
        "rating": "5 star",
        "thumbImg": "/images/devils-crossroad/thumb/devils-cross-thumb.png",
        "offering": {
            "French-Quarter": {
                "image": "/images/devils-crossroad/MuddyWaters-King-Patron.jpg",
                "roomTypes": {
                    "byOccupancy": [
                        "Single"
                    ],
                    "byBed": [
                        "Queen"
                    ],
                    "byLayout": [
                        "Apartment-style"
                    ]
                }
            },
            "Highway-Side": {
                "image": "/images/devils-crossroad/patron-2-queen.jpg",
                "roomTypes": {
                    "byOccupancy": [
                        "Single",
                        "Double"
                    ],
                    "byBed": [
                        "King",
                        "Studio"
                    ],
                    "byLayout": [
                        "Suite"
                    ]
                }
            },
            "Stagger-Lee": {
                "image": "/images/devils-crossroad/patron-4-queen.jpg",
                "roomTypes": {
                    "byOccupancy": [
                        "Single",
                        "Double"
                    ],
                    "byBed": [
                        "Queen",
                        "Studio"
                    ],
                    "byLayout": [
                        "Suite"
                    ]
                }
            },
            "Cotton-top": {
                "image": "/images/devils-crossroad/patron-queen-feature.jpg",
                "roomTypes": {
                    "byOccupancy": [
                        "Single",
                        "Double"
                    ],
                    "byBed": [
                        "Queen",
                        "Studio"
                    ],
                    "byLayout": [
                        "Suite"
                    ]
                }
            }
        }
    }
};