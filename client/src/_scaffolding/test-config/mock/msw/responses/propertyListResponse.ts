export const propertyListResponse = [
    {
        "property": {
            "id": 1,
            "hotelName": "The Round About hotel",
            "country": "US",
            "state": "DC",
            "price": "$320 to  +$1000",
            "thumbImg": "/images/round-about/thumb/round-about-thumb.png"
        }
    },
    {
        "property": {
            "id": 2,
            "hotelName": "The King of Clubs Manor",
            "country": "US",
            "state": "LA",
            "price": "$800 to  +$3000",
            "thumbImg": "/images/king-of-clubs/thumb/king-clubs-thumb.png"
        }
    },
    {
        "property": {
            "id": 3,
            "hotelName": "City Center",
            "country": "US",
            "state": "DC",
            "price": "$200 to $500",
            "thumbImg": "/images/city-center/thumb/city-center-thumb.png"
        }
    },
    {
        "property": {
            "id": 4,
            "hotelName": "Devil's Crossroad Hotel & Spa",
            "country": "US",
            "state": "MS",
            "price": "$1250 to $2250",
            "thumbImg": "/images/devils-crossroad/thumb/devils-cross-thumb.png",
        }
    },
    {
        "property": {
            "id": 5,
            "hotelName": "Dwell Motel",
            "country": "CA",
            "state": "Vancouver",
            "price": "c$120 to c$320",
            "thumbImg": "/images/dwell/thumb/dwell-thumb.png",
        }
    },
    {
        "property": {
            "id": 6,
            "hotelName": "Sweet Dreams",
            "country": "PT",
            "state": "Lisbon",
            "price": "€125 to €230",
            "thumbImg": "/images/sweet-dreams/thumb/sweet-dream-thumb.png",
        }
    },
    {
        "property":  {
            "id": 7,
            "hotelName": "Soho Hostel",
            "country": "UK",
            "state": "London",
            "price": "£140 to £310",
            "thumbImg": "/images/soho-hostel/thumb/soho-thumb.png",
        }
    },
    {
        "property": {
            "id": 8,
            "hotelName": "Saki Okinawa",
            "country": "JP",
            "state": "Okinawa",
            "price": "￥140 to ￥310",
            "thumbImg": "/images/saki-okinawa/thumb/okinawa-thumb.png",
        }
    },
    {
        "property": {
            "id": 9,
            "hotelName": "Nomadic Bohemian",
            "country": "GR",
            "state": "Athina",
            "price": "£32 to £48",
            "thumbImg": "/images/nomadic-bohemian/thumb/bohemian-deck-thumb.png",
        }
    },
    {
        "property":  {
            "id": 10,
            "hotelName": "Coco Cabana Resort & Spa",
            "country": "BR",
            "state": "Cumbuco Beach",
            "price": "R$3300 to R$6300",
            "thumbImg": "/images/coco-cabana/thumb/coco-cabana-thumb.png",
        }
    }
]
