import { rest } from 'msw';
import {
    propertyListResponse,
    propertyDetailResponse
} from './responses';
import { REMOTE_SERVER } from '../../../env';
const API_URL =`${REMOTE_SERVER}/api` ;

const propertyListHandler = rest.get(
    `${REMOTE_SERVER}/api/property/quickList`,
    (req,
     res,
     ctx
    ) => {
        return res((res) =>{
             res.status= 200,
             res.headers.set('Content-Type', 'application/json')
             res.body = {data: propertyListResponse}
                return res;
        });
        
});

const propertyDetailHandler = rest.get(
    `${REMOTE_SERVER}/api/property/:id`,
    (req,
     res,
     ctx
    ) => {
        const fields = req.url.searchParams.get('id');
        //if (fields){
            return res((res) =>{
                res.status= 200,
                res.headers.set('Content-Type', 'application/json')
                res.body = {data: propertyDetailResponse.property}
                return res;
            });
       // }
       //  return res((res) =>{
       //      res.status= 404
       //      return res;
       //  });

    });

export const mswHandler ={
    "propertyListHandler":propertyListHandler,
    "propertyDetailHandler":propertyDetailHandler
};
