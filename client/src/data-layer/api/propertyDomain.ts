import axios, { AxiosResponse, AxiosError } from 'axios';
import {
    PropertyQuickListT,
    PropertyDetailsT
} from "../../business-layer/types";
import { REMOTE_SERVER } from "../../_scaffolding/env";

const instance = axios.create({
    baseURL: `${REMOTE_SERVER}/api`,
    headers: {
        'Content-type': 'application/json',
    },
});

const responseBody = (response: AxiosResponse) => response.data;

const requests = {
    get: (url: string) => instance.get(url).then(responseBody),
    post: (url: string, body: {}) => instance.post(url, body).then(responseBody),
}


export const PropertyDomain = {
    getQuickListOfProperties: async (): Promise<PropertyQuickListT> => {
        const response = await  requests.get('/property/quickList');
        return response.data as PropertyQuickListT;
    },

    getPropertyById: async (id:string): Promise<PropertyDetailsT> => {
        const response = await  requests.get(`${'/property/'+id}`);
        return response.data as PropertyDetailsT;
    },
}
