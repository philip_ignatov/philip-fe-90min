export default ((data:any) => {
  const list = [];
  for (const item in data) {
    const slide = {
      keyName: item,
      image: data[item].image
    }
    list.push(slide);
  }
  return list;
})
