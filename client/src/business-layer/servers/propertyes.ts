import { useQuery } from 'react-query';
import { PropertyDomain } from '../../data-layer/api/propertyDomain'


export const PropertyList = () => {
  const { isLoading, data:response, isError, error, isSuccess } = useQuery('propertyList', () => {
    return PropertyDomain.getQuickListOfProperties();
  }, {
    onError: error => {
      console.log('On error function within query');
      console.log(error);
    },
  })
  return { isLoading, response, isError, error, isSuccess }
}

export const PropertyDetails = (id:string) => {
  const { isLoading, data:response, isError, error, isSuccess, status } = useQuery(['propertyList', id], () => {
    return PropertyDomain.getPropertyById(id);
  }, {
    onError: error => {
      console.log('On error function within query');
      console.log(error);
    },
    enabled: !!id
  })
  return {isLoading, response, status, error, isError, isSuccess }
}
