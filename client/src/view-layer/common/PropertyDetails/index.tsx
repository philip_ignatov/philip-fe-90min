import React from 'react';
import { Box, Heading, Text, SimpleGrid, Flex, Image, Spinner, chakra, Container } from '@chakra-ui/react';
import {PropertyDetails} from "../../../business-layer/servers/propertyes";
import {Carousel} from '../Carousel'
import {
  IProperty,
} from "../../../business-layer/types";

import getSlidesList from '../../../business-layer/local/slidesList'

export const PropertyView = ({ propId, prop}: {propId:string, prop:IProperty}) => {

  const propertyDetailsResponse = PropertyDetails(propId);
  if (propertyDetailsResponse.isLoading) {
    return (<Spinner color='black' size='xl' />)
  }

  let slides = null;
  if (propertyDetailsResponse.isSuccess && propertyDetailsResponse.response) {
    slides = <Carousel slides={getSlidesList(propertyDetailsResponse.response.offering)} />

  }
  if (propertyDetailsResponse.isError) {
    return (
      <Box bg='tomato' w='100%' p={4} color='white'>
        Error
      </Box>
    )
  }

  return (
    <Container>
    {propertyDetailsResponse.isSuccess && propertyDetailsResponse.response &&
      <Box
        mx="0"
        rounded="lg"
        shadow="md"

      >
        {slides}

        <Box p={6}>
          <Box color='black'>
            <chakra.span
              fontSize="xs"
              textTransform="uppercase"
            >
              {prop.hotelName}
            </chakra.span>

            <chakra.p
              mt={2}
              fontSize="sm"
            >
              {propertyDetailsResponse.response.description}
            </chakra.p>

						<chakra.p
							mt={2}
							fontSize="sm"
						>
							Location: {propertyDetailsResponse.response.location}
						</chakra.p>
						<chakra.p
							mt={2}
							fontSize="sm"
						>
							Price: {prop.price}
						</chakra.p>
						<chakra.p
							mt={2}
							fontSize="sm"
						>
							Country: {prop.country + ' ' + prop.state}
						</chakra.p>
          </Box>
        </Box>
    </Box> }
    </Container>

  );
};

