import React from 'react';
import { Box, Progress, Container, Grid, GridItem, VStack, Heading, Text, SimpleGrid, Flex, Image } from '@chakra-ui/react';
import { PropertyList } from '../../../business-layer/servers/propertyes';
import { REMOTE_SERVER } from '../../../_scaffolding/env';
import { PropertyView } from '../PropertyDetails'
import {
  IProperty,
} from "../../../business-layer/types";

export const List = () => {
  const [propId, setPropId] = React.useState('1');
  const [prop, setProp] = React.useState<IProperty>(Object);

  function openPropertyDetails(prop: any) {
    setProp(prop);
    setPropId(prop.id);
  }

  const propertyListResponse = PropertyList();

  let list = null;
  if (propertyListResponse.isSuccess && propertyListResponse.response) {
    list = propertyListResponse.response.map((item) => {
      return (
        <Box
          key={item.id}
          p={3}
          w='100%'
          shadow='md'
          borderWidth='1px'
          borderRadius='10px'
          _hover={{ borderColor: 'red' }}
          onClick={() => openPropertyDetails(item)}
        >
          <Flex alignItems="center">
            <Image
            w={150}
            h={150}
            rounded="full"
            fit="cover"
            display={{ base: "none", sm: "block" }}
            borderRadius='10px'
            src={REMOTE_SERVER + '/' + item.thumbImg}
          />
            <Box p={3}>
              <Heading fontSize='xl'>{item.hotelName}</Heading>
              <Text mt={4}>{item.country + ' ' + item.state}</Text>
              <Text mt={4}>Price: {item.price}</Text>
            </Box>
          </Flex>
        </Box>
      )
    });
  }

  if (propertyListResponse.isLoading) {
    return (<Progress size='xs' isIndeterminate />)
  }

  if (propertyListResponse.isError) {
    return (
      <Box bg='tomato' w='100%' p={4} color='white'>
        Error
      </Box>
    )
  }

  return (

    <SimpleGrid
      columns={{ base: 1, md: 2 }}
      spacing={0}
      _after={{
        bg: "brand.500",
        opacity: 0.25,
        pos: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: -1,
        content: '" "',
      }}
    >
      <Flex
        direction="column"
        alignItems="start"
        justifyContent="center"
        px={{ base: 4, lg: 20 }}
        py={24}
      >
        <VStack spacing={1} isInline={false}>
          {list}
        </VStack>
      </Flex>
      <Flex
        direction="column"
        alignItems="start"

        py={24}
      >
        <Box w='100%' color='white'>
          {propertyListResponse.isSuccess && <PropertyView propId={propId} prop={prop}/>}
        </Box>
      </Flex>
    </SimpleGrid>

  );
};

