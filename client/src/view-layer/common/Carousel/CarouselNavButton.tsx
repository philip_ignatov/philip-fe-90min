import { Box } from '@chakra-ui/react';
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";

export type CarouselNavButtonType = {
    isLeft:boolean;
    handleClick: ()=> void;
}

export const CarouselNavButton = ({isLeft, handleClick}:CarouselNavButtonType) => {
    return isLeft ? (
        <Box
            fontSize="4xl"
            color="white"
            pos="absolute"
            top="45%"
            zIndex="999"
            left="5"
            bg="rgba(0,0,0,0.5)"
            p="3"
            cursor="pointer"
            onClick ={ () => handleClick() }
        >
            <FaChevronLeft/>
        </Box>
    ) : (
        <Box
            fontSize="4xl"
            color="white"
            pos="absolute"
            zIndex="999"
            top="45%"
            right="5"
            bg="rgba(0,0,0,0.5)"
            p="3"
            cursor="pointer"
            onClick ={ () => handleClick() }
        >
            <FaChevronRight/>
        </Box>
    );
};