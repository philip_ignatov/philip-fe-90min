import React, {useEffect, useState} from 'react';
import {
    CarouselNavButton
} from "./CarouselNavButton";
import { REMOTE_SERVER } from "../../../_scaffolding/env";
import {
    Box,
    Flex,
    Image
} from '@chakra-ui/react';

export type SlideT = {
    keyName: string,
    image: string
}

export type CarouselSlidesType = {
    slides: SlideT[]
    postCurrentSlide?:(roomName: string)=>void;
}

function Carousel({slides,  postCurrentSlide }:CarouselSlidesType) {
    const [currentSlide, setCurrentSlide] = useState(0);
    const slidesCount = slides.length;

    const handleMoveLeft = () => {
        const nextSlide = (currentSlide === 0 ? slidesCount - 1 : currentSlide - 1);
        setCurrentSlide( ()=> nextSlide );
        if( postCurrentSlide){
            postCurrentSlide( slides[nextSlide].keyName)
        }
    };

    const handleMoveRight = () => {
        const nextSlide =(currentSlide === slidesCount - 1 ? 0 : currentSlide + 1);
        setCurrentSlide(() => nextSlide);
        if( postCurrentSlide){
            postCurrentSlide( slides[nextSlide].keyName)
        }
    };

    const carouselStyle = {
        transition: "all .5s",
        ml: `-${currentSlide * 100}%`,
    };


    useEffect( ()=>{
        if(slides && currentSlide !== 0){
            setCurrentSlide(0)
        }
    }, [slides])


    return (
        <Box
            display="flex"
            w="full"
            p={5}
            alignItems="center"
            justifyContent="center"
            >
            <Box maxW="container.md" mx="auto" mt="10">
                <Box pos="relative" overflow="hidden">
                    <CarouselNavButton
                        isLeft={true}
                        handleClick={handleMoveLeft}
                    />
                    <CarouselNavButton
                        isLeft={false}
                        handleClick={handleMoveRight}
                    />
                    <Flex w="full" overflow="hidden">
                        <Flex pos="relative" h="400px" w="full" {...carouselStyle}>
                            {slides.map((slide) => (
                                <Box key={slide.keyName} flex="none" boxSize="full" shadow="md">
                                    <Image src={REMOTE_SERVER+slide.image} boxSize="full" backgroundSize="cover" />
                                </Box>
                            ))}
                        </Flex>
                    </Flex>
                </Box>
            </Box>
        </Box>
    );
};

export { Carousel}