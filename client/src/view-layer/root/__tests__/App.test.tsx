import React from 'react';
import { render } from '../../../_scaffolding/test-configs/chakra-test-utils';
import { App } from '../App';

test("Renders App without crashing", () => {
  render(
      <React.StrictMode>
        <App />
      </React.StrictMode>,
  );
});