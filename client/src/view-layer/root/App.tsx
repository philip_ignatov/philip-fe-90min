import * as React from 'react';
import { ChakraProvider, theme } from '@chakra-ui/react';
import { QueryClientProvider,QueryClient } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { List } from '../common/PropertyesList';

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            retry: false,
            staleTime: 30000,
        },
    },
});
export const App = () => (
  <QueryClientProvider client={queryClient}>
    <ChakraProvider theme={theme}>
        <List/>
    </ChakraProvider>
  </QueryClientProvider>
);
