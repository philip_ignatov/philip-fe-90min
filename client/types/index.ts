
export interface IProperty  {
    id: string;
    hotelName: string;
    location: string;
    country: string;
    state: string;
    price: string;
    description: string;
    rating: string;
    thumbImg: string;
    offering: Record<string, unknown>
}
export type PropertyQuickT = Pick<
    IProperty,
    'id'|
    'hotelName'|
    'country'|
    'state'|
    'price'|
    'thumbImg'
    >

export type PropertyListResultT = {
    isLoading?: boolean,
    error?: Error;
    propertyList?: PropertyQuickListT
}

export type PropertyDetailsT = Pick<
    IProperty,
    'id'|
    'description' |
    'location'|
    'offering'
    >

export type PropertyDetailResultT = {
    isLoading?: boolean,
    error?: Error|null;
    setCurrentPropertyId?: (id:string) =>void
}

export type PropertyContentsT = {
    propertyDetail: IProperty |null,
    imageOffering: Record<string, unknown>[] |null,
}

export type PropertyQuickListT =PropertyQuickT[];
