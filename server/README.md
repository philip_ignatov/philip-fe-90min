
# Cloudbeds Frontend 90 minute Live Challenge Server
### Server API Details
####Engine Versions User
<pre>
  $ npm install
  $ npm run start

  // localhost:3500
</pre>

### API
#### Property: server/src/routes/property.js:
* Request property data by Id
    * GET: http://localhost:3500/property/:id
    * router.get('/:id?', auth ...
* Request Quick List  of properties
    * POST: http://localhost:3500/property/quicklist
    * router.get('/list', auth, ...
    
    