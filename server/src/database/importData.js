const jsonData = require('./data.json');
const Property = require('../models/Property')


async function importData() {
  const promises = jsonData.map(
      async(jsonProperty) =>{
          jsonProperty.property.offering = JSON.stringify( jsonProperty.property.offering);
        //  console.log(jsonProperty.property.offering )
          Property.create(jsonProperty.property)
        }
  );
  await Promise.all(promises);
}

module.exports = importData;
