const connection = require('../database/connection');
const importData = require('../database/importData');

async function asyncSetup() {
  await connection.sync({ force: true }).then( async () => {
    console.log(`Database & tables created!`);
    await importData();
  });
}

module.exports = asyncSetup;
