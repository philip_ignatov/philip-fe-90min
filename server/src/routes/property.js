const router = require('express').Router();
const Property = require('../models/Property')

// Model of a user that contains only publicly available properties
class PublicProperty {
  id;
  hotelName;
  location;
  country;
  state;
  price;
  description;
  rating;
  offering;
  createdAt;
  constructor(property) {
    this.id = property.id;
    this.hotelName = property.hotelName;
    this.location = property.location;
    this.country = property.country;
    this.state = property.state;
    this.price = property.price;
    this.rating = property.rating;
    this.thumbImg = property.thumbImg;
    this.offering = property.offering;
    this.createdAt = property.createdAt;
  }
}
const randomTimeInMs =Math.floor((Math.random() * 4000) + 1000);

const functionToExecute = (delay) => console.log(`Ended after ${delay}`)
const executeLater = (functionToExecute, delay) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(functionToExecute(delay))
    }, delay);
  });
}


// Request list of properties
router.get('/property/quickList', async (req, res) => {
  try {
    const pause = await executeLater(functionToExecute, randomTimeInMs)
    if( ( Math.floor(Math.random() * 11) % 4 ) ===0){
        const err = new Error();
        err.message =  "The quicklist is current unavailable";
        err.status = '400';
        err.statusCode = '400';
        return res.status(400).send({data:err})
    }

    const result = await Property.findAll();
    if (!result){
      return res.status(200).send({data:[]});
    }else{
      const quickList = result.map( item => ({
          id: item.id,
          hotelName: item.hotelName,
          country: item.country,
          state: item.state,
          price: item.price,
          thumbImg: item.thumbImg
      }));
      return res.status(200).send({data:quickList});
    }
  } catch (error) {
    res.status(error.status || 500).send({
        status: error.status || 500,
        message: error.message || "Internal Server Error"
    });
  }
})


// Edit an existing property
router.post('/property/:id?', async (req, res) => {
    try {
      const result = await Property.findOne({ where: { id: req.params.id } })
      const propertyExist = result.get({plain:true})
      if( !propertyExist){
        return res.status(400).send("Property does not exists");
      }
      const updatedProperty = await result.update({
        name: req.body.hotelName || propertyExist.hotelName,
        location: req.body.location || propertyExist.location,
        type: req.body.type || propertyExist.type,
        rating:  req.body.rating || propertyExist.rating
      })
      return res.status(200).send(new PublicProperty(updatedProperty));
    } catch( err) {
      return res.status(400).send({data:err})
    }
})

// Edit an existing property
router.get('/property/:id?', async (req, res) => {
  try {
    const result = await Property.findOne({ where: { id: req.params.id } });
    if (!result){
      return res.status(400).send("Property does not exists");
    }else{
      const propertyDetail = {
            id: result.id,
            description: result.description,
            location: result.location,
            offering: JSON.parse(result.offering)
        };
      return res.status(200).send({data:propertyDetail});
    }
  }
  catch( err) {
    console.log(err)
    return res.status(400).send({data:err})
  }
})


module.exports = router;
