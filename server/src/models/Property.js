const { DataTypes } = require('sequelize');
const sequelize = require('../database/connection');

const Property = sequelize.define('Property', {
  // Model attributes are defined here
  hotelName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  location: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  country: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  state: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  price: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  rating: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  thumbImg: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  offering: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
  },

}, {
});

sequelize.sync({ force: true }) // Sync table columns when model gets changed
module.exports = Property;
