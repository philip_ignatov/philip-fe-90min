
require('dotenv').config({ path: "./.env" })
const express = require('express');
const asyncSetup = require('./utils/asyncSetup');
const cors = require('cors');
const app = express();
const { Sequelize } = require('sequelize');
var bodyParser = require('body-parser')

// define a port the app will be listening on
const port = 3500;

// import routes=
const propertyRoute = require('./routes/property')

// middleware'
app.use(cors({
  origin: '*'
}));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'));
app.use(bodyParser.json());

// use routes=
app.use('/api', propertyRoute);


app.ready = asyncSetup(app);

app.listen(port, () => {
  console.log(`App listening on port: ${port}`)
});
