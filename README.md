# Cloudbeds Frontend 90 minute Live Challenge.

## Overview
#### The 2 main goals of this challenge:
* Assess your level of expertise with React, typescript, and other libraries we used in our shop.
* How you structure and compartmentalize (co-locate) code with its associated purpose
    *  ### visual-layer :
        * Strictly UI presentational concerns
        * ( can only access the **business-layer)
    *  ### business-layer :
        * business domain logic concerns associated with communicating  Global/Server state to the
          **business-layer**,
        * instances of cross-cutting concerns such as validators, Domain Models, etc.
        * (has access to **data-layer**)
    *  ### data-layer :
        * Http Client API Implementation,
        * Domain Services,
        * scaffolding for Data stores, etc.

![Alt text](repo-assets/Layerd-High-Level-90-min.png?raw=true "View, Business, Data layer approach" )
### We are building a simple Master-Detail list:
* List of properties on the left side of the screen
  * List of properties requested from the server
* Detailed view of a property on the right
  * Single property requested by id from the server
* When user click on property on the left, a detail view of the  property is revealed on the left side.
![Alt text](repo-assets/90-min-large.gif?raw=true "View, Business, Data layer approach" )

### Tool Sets we use and/or favor in our shop:
* [react-query](https://react-query.tanstack.com/)
* [chakra-ui](https://chakra-ui.com/)
* [testing-library](https://testing-library.com/docs/react-testing-library/intro/)
* [testing-library/react-hooks](https://react-hooks-testing-library.com/)
* [Mock Service Worker](https://mswjs.io/)


**Engine Versions User**
* node:  >= v16.14.0
* npm: >=8.5.0
#### Client  Details
<pre>
  $ cd /client
  $ npm install
  $ npm run start

  // localhost:3000
</pre>


#### Server API Details
<pre>
  $ cd /server
  $ npm install
  $ npm run start

  // localhost:3500
</pre>

**Engine Versions User**
* node:  >= v16.14.0
* npm: >=8.5.0





  🔴 The repo's package.json includes the above libraries. While you can use whatever you want to accomplish your task, 
we do prefer the use of the tools in our package.json.  
  - Since we are accessing this challenge in a 90 minute time period, portions of our code have already been created, so 
  you can focus on implementation and testing.
    - **data-layer**: Domain API Client
    - **business-layer** TypeScript Types.
    - **view-layer**  initial set up App with chakra-ui and react-query providers. The 
    carousel displayed in the property detail view has also been provided.
    - **_saffolding** Testing apparatus ( mock server, chakra-test-utils..etc)
  
### Testing: we embrace testing and part of your assessment is based on your ability to create test
  - Since we are working with in a 90 min timeframe, we will only test what is essential
    - View/Presentational components utilizing business-layer hooks.
    - Hooks in the business-layer, specifically ones associated to Global/Server state 